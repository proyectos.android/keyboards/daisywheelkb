package com.daisywheel.daisywheelkb;

import android.Manifest;
import android.content.pm.PackageManager;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.Toast;

import static androidx.core.app.ActivityCompat.requestPermissions;


public class MyInputMethodService extends InputMethodService implements KeyboardView.OnKeyboardActionListener, KeyEvent.Callback {

    private boolean keyboardOnScreen = false;
    private boolean shortPress = false;
    private boolean longPress = false;
    private long startTime;
    private long elapsedTime;
    private Integer lastPressedKeyCode = null;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public View onCreateInputView() {
        KeyboardView keyboardView = (KeyboardView) getLayoutInflater().inflate(R.layout.keyboard_view, null);
        Keyboard keyboard = new Keyboard(this, R.xml.number_pad);
        keyboardView.setKeyboard(keyboard);
        keyboardView.setOnKeyboardActionListener(this);
        return keyboardView;
    }

    @Override
    public void onPress(int i) {
    }

    @Override
    public void onRelease(int i) {
    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
        InputConnection inputConnection = getCurrentInputConnection();

        if (inputConnection != null) {
            switch(primaryCode) {
                case Keyboard.KEYCODE_DELETE :
                    CharSequence selectedText = inputConnection.getSelectedText(0);

                    if (TextUtils.isEmpty(selectedText)) {
                        inputConnection.deleteSurroundingText(1, 0);
                    } else {
                        inputConnection.commitText("", 1);
                    }

                    break;
                default :
                    char code = (char) primaryCode;
                    inputConnection.commitText(String.valueOf(code), 1);
            }
        }
    }


    @Override
    public void onText(CharSequence charSequence) {
    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {

    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyboardOnScreen){
            startTime = System.nanoTime();
            elapsedTime = System.nanoTime() - startTime;
            event.startTracking();
            if(shortPress) {
                InputConnection inputConnection = getCurrentInputConnection();
                if (inputConnection != null) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_1:
                            try {
                                runShellCommand("input keyevent 26");
                                turnOffScreen();
                            } catch (Exception e) {
                                Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                            break;
                        default:
                            Toast toast1 =
                                    Toast.makeText(getApplicationContext(),
                                            "Short Press " + KeyEvent.keyCodeToString(keyCode), Toast.LENGTH_SHORT);
                            toast1.show();
                            return super.onKeyUp(keyCode, event);
                    }
                }
            }
            shortPress = true;
            longPress = false;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if(keyboardOnScreen){
            Toast.makeText(this, "Long Press" + KeyEvent.keyCodeToString(keyCode), Toast.LENGTH_SHORT).show();
            //Long Press code goes here
            shortPress = false;
            longPress = true;
        }
        return super.onKeyLongPress(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyboardOnScreen){
            event.startTracking();
            if (longPress) {
                shortPress = false;
            } else {
                shortPress = true;
                longPress = false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onFinishInputView(boolean finishingInput) {
        lastPressedKeyCode = null;
        keyboardOnScreen = false;
        super.onFinishInputView(finishingInput);
    }

    @Override
    public void onStartInputView(EditorInfo info, boolean restarting) {
        keyboardOnScreen = true;
        super.onStartInputView(info, restarting);
    }

    private void runShellCommand(String command) throws Exception {
        Process process = Runtime.getRuntime().exec(command);
        process.waitFor();
    }

    private void turnOffScreen() {
        int defaultTurnOffTime = Settings.System.getInt(getContentResolver(),Settings.System.SCREEN_OFF_TIMEOUT, 60000);
        Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_OFF_TIMEOUT, 1);
        Settings.System.putInt(getContentResolver(),Settings.System.SCREEN_OFF_TIMEOUT, defaultTurnOffTime);
    }
}