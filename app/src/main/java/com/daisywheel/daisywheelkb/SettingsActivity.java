package com.daisywheel.daisywheelkb;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.preference.PreferenceFragment;

public class SettingsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // New permissions model test
/*        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            Toast.makeText(getApplicationContext(), Build.VERSION.SDK_INT + ">=" + Build.VERSION_CODES.M, Toast.LENGTH_SHORT).show();
            if (checkSelfPermission(Manifest.permission.WRITE_SECURE_SETTINGS) != PackageManager.PERMISSION_GRANTED)
            {
                String [] permissions = new String[1];
                permissions[0] = Manifest.permission.WRITE_SECURE_SETTINGS;

                requestPermissions(permissions, 7001);
            }
        }*/
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_fragment);
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}